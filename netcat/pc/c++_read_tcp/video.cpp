#include <string>
#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
	namedWindow("Example3", cv::WINDOW_AUTOSIZE);
	VideoCapture cap;
	cap.open("http://192.168.226.101:8080/video?x.mjpeg");
	if (!cap.isOpened())  // if not success, exit program
    {
        cout << "Cannot open the video cam" << endl;
        return -1;
    }
	Mat frame;
	for(;;) {
		cap >> frame;
		if (frame.empty()) break;	// Range out of film
		imshow("Example3", frame);
		if ( (cv::waitKey(33) & 0xFF) == 27) {
			std::cout << "You press ESC!" << std::endl;
		   	break;
		}
	}
	return 0;
}
