# CMake generated Testfile for 
# Source directory: /home/pi/CS/compile_and_install_OpenCV/opencv-3.2.0/samples
# Build directory: /home/pi/CS/compile_and_install_OpenCV/opencv-3.2.0/build/samples
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("cpp")
subdirs("gpu")
subdirs("tapi")
subdirs("python")
