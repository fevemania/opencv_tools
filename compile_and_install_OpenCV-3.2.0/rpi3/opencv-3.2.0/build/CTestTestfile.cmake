# CMake generated Testfile for 
# Source directory: /home/pi/CS/compile_and_install_OpenCV/opencv-3.2.0
# Build directory: /home/pi/CS/compile_and_install_OpenCV/opencv-3.2.0/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("3rdparty/libwebp")
subdirs("3rdparty/openexr")
subdirs("include")
subdirs("modules")
subdirs("doc")
subdirs("data")
subdirs("apps")
subdirs("samples")
