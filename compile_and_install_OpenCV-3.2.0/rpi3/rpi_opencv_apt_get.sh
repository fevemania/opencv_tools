# this script is for installing opencv 3.2.0 on RPI (with some optimized)

# the version of RPI: Linux raspberrypi 4.9.41-v7+ #1023 SMP Tue Aug 8 16:00:15 BST 2017 armv7l GNU/Linux

#first step is to install required packages

# you can use "dpkg --get-selections | grep xxx" to inspect 
# if there is the package named xxx already installed. 


sudo apt-get install -y build-essential

sudo apt-get install -y checkinstall htop swig ccache doxygen

#[optimized some matrix operations]
sudo apt-get -y install libatlas-base-dev gfortran

#[ccmake]
sudo apt-get install -y cmake-curses-gui

sudo apt-get install -y cmake libgtk2.0-dev pkg-config

#[ffmpeg or libav development packages]
sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev libavresample-dev

#[optional]
sudo apt-get install -y python-dev python-numpy python3-dev python3-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev \
	libtiff5-dev libjasper-dev zlib1g zlib1g-dev libv4l-dev


#[customized install] 
sudo apt-get install -y libeigen3-dev

sudo apt-get install -y libgstreamer1.0-0 libgstreamer1.0-dev gstreamer1.0-tools \
	gstreamer1.0-plugins-base libgstreamer-plugins-base1.0 \
	gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-plugins-bad gstreamer1.0-libav \
	libx264-dev x264 libxvidcore-dev

# Step2. 
# go into `opencv-3.2.0` folder
# mkdir build
# cd build
# ccmake .. to tune for following flags or simply run ./rpi_opencv_cmake_file.sh
#
# !! NOTE:  -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-3.2.0/modules should revise to your own contrib path

# BUILD-EXAMPLES OFF
# INSTALL C Examples OFF
# INSTALL python examples ON
# WITH 1394 OFF
# With_CAROTENE OFF
# WITH_CUDA set OFF
# WITH_CUFFT off
# WITH_EIGEN: ON 
# WITH_GPHOTO2 OFF
# WITH_MATLAB OFF
# WITH PNG set ON
# WITH QT set OFF
# WITH TBB ON (self decide)

# to get any v4l support, you must compile with WITH_V4L = ON, as libv4l is deprecated also set WITH_LIBV4L=OFF

# when BUILD_EXAMPLES and INstall C Examples, the check for `sys/videoio.h` would be not found, and it cause error.

# I am not going to use IEEE 1394 camera interface, so we set WITH 1394 OFF GPhoto2 OFF
