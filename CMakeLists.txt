cmake_minimum_required(VERSION 2.8)
project(c++_opencv)
find_package(OpenCV REQUIRED)

add_executable(check check_opencv_flags.cpp)

target_link_libraries(check ${OpenCV_LIBS})
