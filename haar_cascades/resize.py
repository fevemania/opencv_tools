import cv2
import os
import sys

if len(sys.argv) != 3:
    print(sys.argv[0], '<source folder> <dst folder>')
    sys.exit(1)

directory = os.fsencode(sys.argv[1])

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".png"):
        #print(os.path.join(sys.argv[1], filename))
        file_path = os.path.join(sys.argv[1], filename)
        img = cv2.imread(file_path)
        img = cv2.resize(img, (80, 80))
        
        dst_path = os.path.join(sys.argv[2], filename)
        cv2.imwrite(dst_path, img)
