#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <vector>
#include <glob.h>
#include <iostream>
#include <string>
using namespace cv;
using namespace std;
static void help(char* filename) {
	printf("using: %s <source_img_folder> <output_img_folder>", filename);
}
int main(int argc, char** argv)
{
	if (argc != 3) {
		help(argv[0]);
		return -1;
	}
	if (!strcmp(argv[1], argv[2])) {
		printf("two folder can't be the same one.");
		return -1;
	}
	

	std::vector<cv::String> filenames;
	cv::String folder = argv[1];
	std::string dst_folder(argv[2]);
	
	glob(folder, filenames);

	for(size_t i = 0; i < filenames.size(); ++i)
    {
		Mat img = imread(filenames[i], -1);
		if (img.empty()) {
			// Check if there is file .DS_Store in src folder -> such an annoying file exist in MacOs!
			string str = argv[1];
			str += "/.DS_Store";
			if (str == filenames[i])
				continue;
			std::cerr << "Problem loading image!!!" << std::endl;
			return -1;
		}
		
		// set dst_path
    	string filename_with_old_extension = filenames[i].substr(strlen(argv[1])+1, filenames[i].length());   	
    	string key = ".";
    	string dst_path = "";

    	std::size_t found = filename_with_old_extension.rfind(key);  // If no matches were found, the function returns string::npos.
    	if (found != std::string::npos) {
    		string filename = filename_with_old_extension.substr(0, found);
    		
    		dst_path = dst_folder + "/" + filename + ".png";
    	}
    	else {
    		std::cerr << "can't find extension name." << std::endl;
    		return -1;
    	}
		
		cv::imwrite(dst_path, img);
    }
	return 0;
}
