#include <opencv2/opencv.hpp>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;
using namespace cv;

int main(int argc, char** argv) {

	int delay = 5;
	cv::namedWindow("2_10", cv::WINDOW_AUTOSIZE);

	cv::VideoCapture cap;
	if (argc == 1) {
		cap.open(0);	   // open the first camera
	} else {
		cap.open(argv[1]); // if filename is supplied, Opencv read video file.
	}
	if ( !cap.isOpened() ) {  // check if we succeeded
		std::cerr << "Couldn't open capture." << std::endl;
		return -1;
	}
	
	// set fps	
	cap.set(CAP_PROP_FRAME_WIDTH, 640);
	cap.set(CAP_PROP_FRAME_HEIGHT, 480);
	// cap.set(CAP_PROP_FPS, 20);

	waitKey(delay);

	cv::Mat frame;
	for(;;) {
		cap >> frame;
		if (frame.empty()) break;	// Range out of film
		
		cout << "fps: " << cap.get(CAP_PROP_FPS) << endl;
		cout << "frame_width: " << cap.get(CAP_PROP_FRAME_WIDTH) << endl;
		cout << "frame_high: " << cap.get(CAP_PROP_FRAME_HEIGHT) << endl;
		cout << "bright: " << cap.get(CAP_PROP_BRIGHTNESS) << endl;
		
		cv::imshow("2_10", frame);
		if ( (cv::waitKey(delay) & 0xFF) == 27) {
			std::cout << "You press ESC!" << std::endl;
		   	break;
		}
	}
	return 0;
}
