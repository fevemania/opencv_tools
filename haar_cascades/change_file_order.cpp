#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <vector>
#include <glob.h>
#include <iostream>
#include <string>
using namespace std;
using namespace cv;

static void help(char* filename) {
	printf("using: %s <src_img_folder> <dst_img_folder> <filename> <start_number>\n\n", filename);
	printf("Ex. %s jpg png bottle 1\n\n", filename);
	printf("Then output would be:\n");
	printf("png/bottle1.png(or png/bottle1.jpg)\n"
		"png/bottle2.png(or png/bottle2.jpg)\n"
		"png/bottle3.png(or png/bottle3.jpg)\n");
}

int main(int argc, char** argv)
{
	if (argc != 5) {
		help(argv[0]);
		return -1;
	}
	if (!strcmp(argv[1], argv[2])) {
		printf("two folder can't be the same one.");
		return -1;
	}
	

	std::vector<cv::String> filenames;
	cv::String folder = argv[1];
	std::string dst_folder(argv[2]);
	string new_filename_title(argv[3]);

	glob(folder, filenames);
	int count;
	std::stringstream s;
	s << argv[4];
	s >> count;

	for(size_t i = 0; i < filenames.size(); ++i)
    {
		Mat img = imread(filenames[i], -1);
		if (img.empty()) {
			// Check if there is file .DS_Store in src folder -> such an annoying file exist in MacOs!
			string str = argv[1];
			str += "/.DS_Store";
			if (str == filenames[i])
				continue;
			std::cerr << "Problem loading image!!!" << std::endl;
			return -1;
		}
		
		// set dst_path
    	string old_filename = filenames[i].substr(strlen(argv[1])+1, filenames[i].length());   	
    	string key = ".";
    	string dst_path = "";

    	std::size_t found = old_filename.rfind(key);  // If no matches were found, the function returns string::npos.
    	if (found != std::string::npos) {
    		string extension_name = old_filename.substr(found, old_filename.length());
    		string cnt = std::to_string(count);
    		dst_path = dst_folder + "/" + new_filename_title + cnt + extension_name;
    	}
    	else {
    		std::cerr << "can't find extension name." << std::endl;
    		return -1;
    	}
		
		cv::imwrite(dst_path, img);
		++count;
    }
	return 0;
}
