import numpy as np
import cv2
from scipy.signal import savgol_filter


# Step 0: first begin with preprocessing the image with a slight Gaussian blur to reduce noise from the original image
# before doing an edge detection
def simple_blur(img):
    blurred = cv2.GaussianBlur(img, (5,5), 0)  # Remove noise
    return blurred


# Step 1: Apply the most commonly use method to do edge detection # is to use the Sobel operator.

# Note. Dong this with loops in python would be slow. np.hypot() method can do that for us quickly!
def edgedetect(channel):
    sobelX = cv2.Sobel(channel, cv2.CV_16S, 1, 0)
    sobelY = cv2.Sobel(channel, cv2.CV_16S, 0, 1)
    sobel = np.hypot(sobelX, sobelY)
    sobel[sobel > 255] = 255  # saturate cast
    return sobel

# Since we are dealing with color images, the edge detection needs to be run on each color channel and then they need to be combined.

# The combination way I am doing that is by finding the max intesity from among the R,G and B edges.
# It seems like max gives better results than average.



def findSignificantContours (img, edgeImg):
    #image, contours, heirarchy = cv2.findContours(edgeImg, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    image, contours, heirarchy = cv2.findContours(edgeImg, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Find level 1 contours
    level1 = []
    for i, tupl in enumerate(heirarchy[0]):
        # Each array is in format (Next, Prev, First child, Parent)
        # Filter the ones without parent
        if tupl[3] == -1:
            tupl = np.insert(tupl, 0, [i])
            level1.append(tupl)
    # From among them, find the contours with large surface area.
    significant = []
    tooSmall = edgeImg.size * 5 / 100 # If contour isn't covering 5% of total area of image then it probably is too small
    for tupl in level1:
        contour = contours[tupl[0]];

        #Contour smoothing
        epsilon = 1*cv2.arcLength(contour,True)
        approx = cv2.approxPolyDP(contour, 0,True)
        contour = approx

        area = cv2.contourArea(contour)
        if area > tooSmall:
            significant.append([contour, area])

            # Draw the contour on the original image
            cv2.drawContours(img, [contour], 0, (0,0,0), 2, cv2.LINE_AA, maxLevel=0)

    significant.sort(key=lambda x: x[1])
    #print ([x[1] for x in significant]);
    return [x[0] for x in significant];

if __name__ == '__main__':
    for count in range(1,61):
        img = cv2.imread('./dst_positive_images/bottle'+str(count)+'.jpg')
        #img = cv2.imread('./dst_positive_images/bottle0.png')
        blurred = simple_blur(img)
        #print(type(edgedetect(blurred[:,:,0])))
        edgeImg = np.max(np.array([edgedetect(blurred[:,:, 0]), edgedetect(blurred[:,:, 1]), edgedetect(blurred[:,:, 2])]), axis=0)
        mean = np.mean(edgeImg);

        # Zero any value that is less than mean. This reduces a lot of noise.
        edgeImg[edgeImg <= mean] = 0;

        edgeImg_8u = np.asarray(edgeImg, np.uint8)
        significant = findSignificantContours(img, edgeImg_8u)
        # Mask
        mask = edgeImg.copy()
        #mask[mask > 0] = 255 
        mask[mask > 0] = 0 
        cv2.fillPoly(mask, significant, 255)
        # Invert mask
        mask = np.logical_not(mask)

        #Finally remove the background
        img[mask] = 0;
        #img[mask] = 255;
        

        #cv2.imshow('edge', dst)
        cv2.imwrite('./rm_background_images/bottle'+str(count)+'.jpg', img)
        #cv2.imwrite('bottle_test.png', img)
        #cv2.destroyAllWindows()
