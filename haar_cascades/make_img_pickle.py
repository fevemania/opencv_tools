import os
import sys
import numpy as np
import glob
from PIL import Image
import pickle

source_directory = sys.argv[1]
pickle_file = sys.argv[2]

if len(sys.argv) != 3:
    print(sys.argv[0], '<source folder> pickle_name')
    print("ex. ", sys.argv[0], '<source folder> test.p')
    sys.exit(1)

filelist = glob.glob(os.path.join(source_directory, '*.png'))
x = np.array([np.array(Image.open(filename)) for filename in filelist])

try:
    with open(pickle_file, 'wb') as pfile:
        pickle.dump(x, pfile, protocol=2)
except Exception as e:
    print('Unable to save data to', pickle_file, ":", e)
    raise


print('Data cached in ', pickle_file, '.')
