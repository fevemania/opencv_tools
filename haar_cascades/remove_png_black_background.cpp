// This program assume that the background pixel is either 0 in grayscale or (0,0,0) in bgr
// This program will convert other img type to png in order to set background transparent

#include <opencv2/opencv.hpp>  // Include file for every supported OpenCV function
#include <glob.h>
#include <stdio.h>
using namespace cv;
using namespace std;
static void help(char* filename) {
	printf("using: %s <source_img_folder> <output_img_folder>\n", filename);
}
int main(int argc, char** argv) {
	if (argc != 3) {
		help(argv[0]);
		return -1;
	}
	if (!strcmp(argv[1], argv[2])) {
		printf("two folder can't be the same one.");
		return -1;
	}

	std::vector<cv::String> filenames;
	cv::String folder = argv[1];
	std::string dst_folder(argv[2]);

	glob(folder, filenames);

	for(size_t i = 0; i < filenames.size(); ++i)
    {

		Mat img = imread(filenames[i], -1);
		if (img.empty()) {
			// Check if there is file .DS_Store in src folder -> such an annoying file exist in MacOs!
			string str = argv[1];
			str += "/.DS_Store";
			if (str == filenames[i])
				continue;
			std::cerr << "Problem loading image!!!" << std::endl;
			return -1;
		}
		
    	// set dst_path
    	string filename_with_old_extension = filenames[i].substr(strlen(argv[1])+1, filenames[i].length());   	
    	string key = ".";
    	string dst_path = "";

    	std::size_t found = filename_with_old_extension.rfind(key);  // If no matches were found, the function returns string::npos.
    	if (found != std::string::npos) {
    		string filename = filename_with_old_extension.substr(0, found);
    		
    		dst_path = dst_folder + "/" + filename + ".png";
    	}
    	else {
    		std::cerr << "can't find extension name." << std::endl;
    		return -1;
    	}

		if(img.channels()==1)
			cv::cvtColor(img, img, CV_GRAY2BGR);
		cv::cvtColor(img, img, CV_BGR2BGRA);
		for (cv::Mat4b::iterator it = img.begin<cv::Vec4b>(); it != img.end<cv::Vec4b>(); it++) {
			if (*it == cv::Vec4b(0, 0, 0, 255)) {
				*it = cv::Vec4b(0, 0, 0, 0);
			}
		}

		imwrite(dst_path, img);
	}
	return 0;
}


