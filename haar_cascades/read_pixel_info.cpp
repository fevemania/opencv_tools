#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
using namespace std;
using namespace cv;

void read_pixel_info(Mat &img, Point pixel)
{
	int nChannel = img.channels();
	printf("at (%d, %d) value is: \n", pixel.x, pixel.y);	
	if (nChannel == 1) {
		cout << static_cast<int>(img.at<uchar>(pixel)) << endl;	
	}
	else if (nChannel == 3) {
		Vec3b& bgr = img.at<Vec3b>(pixel);
		cout << bgr << endl;
	}
	else if (nChannel == 4) {	
		Vec4b& bgra = img.at<Vec4b>(pixel);
		cout << bgra << endl;
	}
}
// Use mouse to get pixel position
void onMouse(int event, int x, int y, int flags, void* param) {
	cv::Mat *img = reinterpret_cast<cv::Mat*>(param);
	switch (event) {
		case cv::EVENT_LBUTTONDOWN:
			read_pixel_info(*img, Point(x,y));
			break; 	
	}
}

static void help(char* filename) {
	printf("using: %s <img_file>\n\n", filename);
}

int main(int argc, char** argv) {
	if (argc != 2) {
		help(argv[0]);
		return -1;
	}
	namedWindow("img", cv::WINDOW_AUTOSIZE);
	Mat img = imread(argv[1], IMREAD_UNCHANGED);
//	Mat img = imread(argv[1], 0);
	if (img.empty()) return -1;
	printf("num_channel: %d\n", img.channels());
	cv::setMouseCallback("img", onMouse, reinterpret_cast<void*>(&img));
	imshow("img", img);
	waitKey(0);
	return 0;
}
