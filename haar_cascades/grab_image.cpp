// This program grab size (640,480) picture and save in .png format

#include <opencv2/opencv.hpp>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;
using namespace cv;

static void help(char* filename) {
	printf("using: %s <folder_to_store_img> <filename> <start_number>\n\n", filename);
	printf("Ex. %s png bottle 1\n\n", filename);
	printf("Then output would be:\n");
	printf("png/bottle1.png(or png/bottle1.png)\n"
		"png/bottle2.png(or png/bottle2.png)\n"
		"png/bottle3.png(or png/bottle3.png)\n");
}

int main(int argc, char** argv) {
	if (argc != 4) {
		help(argv[0]);
		return -1;
	}

	int delay = 5;
	cv::namedWindow("2_10", cv::WINDOW_AUTOSIZE);

	cv::VideoCapture cap;
	
	cap.open(0);	   // open the first camera
	
	if ( !cap.isOpened() ) {  // check if we succeeded
		std::cerr << "Couldn't open capture." << std::endl;
		return -1;
	}
	
	std::string dst_folder(argv[1]);
	string filename_title(argv[2]);

	int count;
	std::stringstream s;
	s << argv[3];
	s >> count;

	// set fps	
	cap.set(CAP_PROP_FRAME_WIDTH, 640);
	cap.set(CAP_PROP_FRAME_HEIGHT, 480);
	cap.set(CAP_PROP_FPS, 30);
	waitKey(1000);

	cv::Mat frame;
	for(;;) {
		cap >> frame;
		if (frame.empty()) break;	// Range out of film
		
		// if(frame.channels()==1)
			// cv::cvtColor(frame, frame, CV_GRAY2BGR);
		cv::cvtColor(frame, frame, CV_BGR2BGRA);

		cv::imshow("2_10", frame);
		string dst_path = dst_folder + "/" + filename_title + std::to_string(count) + ".png";
		cout << dst_path << endl;
		cv::imwrite(dst_path, frame);
		++count;
		if ( (cv::waitKey(delay) & 0xFF) == 27) {
			std::cout << "You press ESC!" << std::endl;
		   	break;
		}
	}
	return 0;
}
